#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, ModelSingleton, fields


class Configuration(ModelSingleton, ModelSQL, ModelView):
    _name = 'party.configuration'

    party_event_type = fields.Property(fields.Many2One('party.event.type',
        'Party Event Type'))

Configuration()
