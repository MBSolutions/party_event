# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import datetime
from trytond.model import ModelView, ModelSQL, fields
from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond.ir.attachment import firstline


class EventType(ModelSQL, ModelView):
    'Event Type'
    _name = 'party.event.type'
    _description = __doc__

    name = fields.Char('Name', required=True, translate=True, loading='lazy',
        help='The name of the event type.')
    code = fields.Char('Code', required=True,
        help='The code of the event type.')
    description = fields.Text('Description',
        help='The description of the event type.')
    system_type = fields.Selection([
            ('note', 'Note'),
            ], 'System Type', required=True,
        help='The internal type this event type is based on.')
    sequence = fields.Integer('Sequence')

    def __init__(self):
        super(EventType, self).__init__()
        self._sql_constraints += [
            ('code_uniq', 'UNIQUE(code)', 'The code must be unique!'),
            ]
        self._order.insert(0, ('sequence', 'ASC'))
        self._order.insert(1, ('name', 'ASC'))

    def default_system_type(self):
        return 'note'

    def default_sequence(self):
        return 10

EventType()


class Event(ModelSQL, ModelView):
    'Event'
    _name = 'party.event'
    _description = __doc__
    _rec_name = 'summary'

    summary = fields.Char('Summary', states={'readonly': True},
        on_change_with=['description'], depends=['description'],
        help='The subsumption of the event. It is auto filled with the '
        'first line of the Description field')
    description = fields.Text('Description', required=True,
        help='The description of the event. The first line of the '
        'description field is filled in the Summary field.')
    parties = fields.Many2Many('party.event-party.party', 'event', 'party',
        'Parties',
        help='The parties involved in this event.')
    time = fields.DateTime('Time', required=True,
        help='Point in time for this event.')
    type = fields.Selection('get_types', 'Type', required=True,
        help='The type of the event.\n'
        'The Type options and default type of this selection are defined '
        'in the Party Configuration.')
    references = fields.One2Many('party.event.reference', 'event',
        'References',
        help='The records from other models linked to this event.')
    reference_count = fields.Function(
        fields.Integer('Reference Count',
            on_change_with=['references'],
            help='The number of references for this event.',
            ),
        'get_reference_count')

    def __init__(self):
        super(Event, self).__init__()
        self._order.insert(0, ('time', 'DESC'))

    def default_time(self):
        return datetime.datetime.now().replace(microsecond=0)

    def default_type(self):
        config_obj = Pool().get('party.configuration')
        event_type_obj = Pool().get('party.event.type')

        config = config_obj.browse(1)
        if config.party_event_type:
            return config.party_event_type.code
        with Transaction().set_context(language='en_US'):
            note_type_ids = event_type_obj.search([
                ('system_type', '=', 'note'),
                ('name', '=', 'Note'),
                ])
        return event_type_obj.browse(note_type_ids[0]).code

    def default_parties(self):
        res = []
        if Transaction().context.get('party'):
            res.append(Transaction().context.get('party'))
        return res

    def get_types(self):
        type_obj = Pool().get('party.event.type')
        type_ids = type_obj.search([])
        types = type_obj.browse(type_ids)
        return [(x.code, x.name) for x in types]

    def on_change_with_reference_count(self, vals):
        if vals.get('references'):
            return len(vals['references'])
        return 0

    def on_change_with_summary(self, values):
        return firstline(values.get('description') or '')

    def get_reference_count(self, ids, name):
        res = {}
        for event in self.browse(ids):
            if event.references:
                res[event.id] = len(event.references)
            else:
                res[event.id] = 0
        return res

Event()


class EventParty(ModelSQL):
    'Event - Party'
    _name = 'party.event-party.party'
    _description = __doc__
    _rec_name = 'event'

    party = fields.Many2One('party.party', 'Party', ondelete='CASCADE')
    event = fields.Many2One('party.event', 'Event')

EventParty()


class EventLink(ModelSQL, ModelView):
    'Event Link'
    _name = 'party.event.link'
    _description = __doc__

    name = fields.Char('Name', required=True, translate=True, loading='lazy')
    model = fields.Selection('get_models', 'Model', required=True)
    sequence = fields.Integer('Sequence')

    def __init__(self):
        super(EventLink, self).__init__()
        self._order.insert(0, ('sequence', 'ASC'))

    def default_sequence(self):
        return 10

    def get_models(self):
        model_obj = Pool().get('ir.model')

        model_ids = model_obj.search([])
        res = []
        for model in model_obj.browse(model_ids):
            res.append((model.model, model.name))
        return res

EventLink()


class EventReference(ModelSQL, ModelView):
    'Event Reference'
    _name = 'party.event.reference'
    _description = __doc__
    _rec_name = 'reference'

    event = fields.Many2One('party.event', 'Event', required=True,
        ondelete="CASCADE", select=1)
    reference = fields.Reference('Reference', selection='get_links',
        required=True)
    sequence = fields.Integer('Sequence')

    def __init__(self):
        super(EventReference, self).__init__()
        self._order.insert(0, ('sequence', 'ASC'))

    def default_sequence(self):
        return 10

    def get_links(self):
        event_link_obj = Pool().get('party.event.link')

        ids = event_link_obj.search([])
        event_links = event_link_obj.browse(ids)
        return [(x.model, x.name) for x in event_links]

EventReference()
